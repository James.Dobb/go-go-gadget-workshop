/*
	!!! DO NOT MODIFY THIS FILE !!!
*/

package work

import "time"

const totalPages = 1000000

func NewFolder() *Folder {
	return &Folder{
		createdAt: time.Now(),
		reports:   []Report{},
	}
}

type Folder struct {
	createdAt time.Time
	reports   []Report
}

func (f *Folder) AddReport(r Report) {
	f.reports = append(f.reports, r)
}

func (f *Folder) CompletedReports() int {
	return len(f.reports)
}

func (f *Folder) EmptyPages() int {
	return totalPages - f.CompletedReports()
}

func (f *Folder) ElapsedTime() float64 {
	return time.Now().Sub(f.createdAt).Seconds()
}