package main

import (
	"fmt"
	"sleepy-craig/craig"
	"sleepy-craig/work"
)

/*
Craig has lots of work to do; 1,000,000 reports to be delivered!

Unfortunately Craig has a habit of sleeping on the job.  Its going to take quite
some time for him to finish at this rate!

You cannot modify Craig or remove his power naps, but perhaps there is something you
could do to speed this up?

No modifying anything outside this file.... or any other type of cheating.  As sleepy
as Craig is he must complete the reports; no forging them!
 */
func handle(c *craig.Craig, f *work.Folder)  {
	for i := 0; i < f.EmptyPages(); i++ {
		f.AddReport(c.PerformTask())
		fmt.Print(fmt.Sprintf("\rCraig has completed %v reports", f.CompletedReports()))
	}
}
