/*
	!!! DO NOT MODIFY THIS FILE !!!
*/

package craig

import (
	"sleepy-craig/work"
	"time"
)

type Craig struct {}

func New() *Craig {
	return &Craig{}
}

func (c Craig) PerformTask() work.Report {
	time.Sleep(time.Millisecond * 200)
	return work.Report{}
}