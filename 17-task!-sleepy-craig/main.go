/*
	!!! DO NOT MODIFY THIS FILE !!!
*/

package main

import (
	"fmt"
	"sleepy-craig/craig"
	"sleepy-craig/work"
)

func main() {
	c := craig.New()
	f := work.NewFolder()

	handle(c, f)

	emptyPages := f.EmptyPages()
	if emptyPages != 0 {
		panic(fmt.Sprintf("you have %v empty pages left, you should have 0, Craig!", emptyPages))
	}

	fmt.Printf("Craig completed his reports in %v seconds\n", f.ElapsedTime())
}
