package main

import "fmt"

const x1 = 1

const (
	x2 = 2
	x3 = 3
)

func printConstants()  {
	fmt.Println(x1, x2, x3)
}