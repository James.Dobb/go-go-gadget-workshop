package main

import (
	"fmt"
)

func explicitType()  {
	var explicitType string
	explicitType = "explicit type"

	fmt.Println(explicitType)
}

func implicitType()  {
	implicitType := "implicitType type"

	fmt.Println(implicitType)
}

var globalVariable = "global"
func globals()  {
	fmt.Println(globalVariable)

	// note the "=" to indicate we're setting the global variable
	globalVariable = "updated.global"
	fmt.Println(globalVariable)

	// note the ":=" to indicate we're creating a new variable
	globalVariable := 2
	fmt.Println(globalVariable)

	// this won't work, we cannot change the variable type, local variables win in a conflict
	// globalVariable = "hi"
}

func multiAssignment() {
	var1, var2, var3 := "one", "two", 3

	fmt.Println(var1, var2, var3)
}
