package main

import "fmt"

func main()  {
	explicitType()
	fmt.Println("---")

	implicitType()
	fmt.Println("---")

	globals()
	fmt.Println("---")

	printConstants()
}