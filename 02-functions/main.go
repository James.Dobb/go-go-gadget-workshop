package main

import "fmt"

// use: go run .
// go run main.go will not work here!
func main()  {
	printInput("hello, world")
	printDoubleInput("hello,", "world")
	printDoubleInputAssumedType("hello,", "world")
	printVariadicInput("hello,", "world", "!")

	fmt.Println(singleReturn())
	fmt.Println(multiReturn())
	handlingReturnVariables()
}