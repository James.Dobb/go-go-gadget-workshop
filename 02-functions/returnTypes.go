package main

import "fmt"

func singleReturn() string {
	return "single return"
}

func multiReturn() (string, int) {
	return "multi return", 42
}

func handlingReturnVariables() {
	// if you have no use for return variables, assigning them will cause a compilation error
	// x, y := multiReturn()

	// you can entirely ignore returns
	// (bad in most cases)
	multiReturn()

	// you can use "_" to ignore only parts of a return that you don't use
	// (still bad in most cases, although exceptions exist)
	rtnStr, _ := multiReturn()
	fmt.Println("i would like to handle: ", rtnStr)

	// you can also use "_" to ignore all return variables
	// (still bad, but provides better visibility)
	_, _ = multiReturn()
}
