package main

import "fmt"

func printInput(input string) {
	fmt.Println(input)
}

func printDoubleInput(input1 string, input2 string)  {
	fmt.Println(input1, input2)
}

func printDoubleInputAssumedType(input1, input2 string) {
	fmt.Println(input1, input2)
}

// Variadic input
func printVariadicInput(input1 string, inputN ...string) {
	fmt.Println(input1, inputN) // note the print output, this is a slice!
}

