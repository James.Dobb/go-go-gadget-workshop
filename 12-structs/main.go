package main

import "fmt"

type Car struct {
	name string
	maxSpeed float32
}

func (c *Car) increaseMaxSpeed() {
	c.maxSpeed += 1
}

func (c Car) increaseMaxSpeedWithoutPointer() {
	c.maxSpeed += 1
}

func main() {
	mustang := Car{
		name:          "horse-car",
		maxSpeed:      56.7,
	}

	fmt.Println(mustang.name, mustang.maxSpeed)

	mustang.increaseMaxSpeed()
	fmt.Println(mustang.name, mustang.maxSpeed)

	mustang.increaseMaxSpeedWithoutPointer()
	fmt.Println(mustang.name, mustang.maxSpeed)
}