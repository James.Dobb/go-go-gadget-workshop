package main

import (
	"errors"
)

type Doggo struct {
	name             string
	breed            string
	weight           map[int]int
	fluffinessRating string
	favoriteTreat    string
}

// Name : this looks straight forward enough
func (d Doggo) Name() (string, error) {
	return "", errors.New("out of chimken")
}

// Breed : hmmm values are stored in a slightly unexpected format e.g.: 476f6f6420426f79
// It's almost like someone hexed them...
func (d Doggo) Breed() (string, error) {
	return "", errors.New("monster squirrel!")
}

// Weight : expect to return in KG to 1dp
// It looks like the input is an ordered map of numbers
// I wonder what the key represents?
func (d Doggo) Weight() (float64, error) {
	return 0, errors.New("vets")
}

// Fluffiness : the return type looks different
func (d Doggo) Fluffiness() (int, error) {
	return 0, errors.New("cats")
}

// FavoriteTreat : seems like we're backwards!
func (d Doggo) FavoriteTreat() (string, error) {
	return "", errors.New("tripe")
}
