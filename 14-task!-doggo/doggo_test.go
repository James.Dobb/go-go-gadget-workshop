package main

import (
	"github.com/maxatome/go-testdeep/td"
	"testing"
)

var doggos = map[string]Doggo{
	"nova": {
		name:        "Nova",
		breed:       "4c616e6420536861726b",
		weight: map[int]int{
			0: 1,
			1: 3,
			2: 6,
		},
		fluffinessRating: "ten",
		favoriteTreat: "sineP slluB",
	},
	"rover": {
		name:        "Rover",
		breed:       "476f6f6420426f79",
		weight: map[int]int{
			2: 1,
			1: 3,
			0: 2,
		},
		fluffinessRating: "five",
		favoriteTreat: "senoB yvarG",
	},
	"pavlova": {
		breed:       "G476f6f6420426f79",
		weight: map[int]int{
			7:  1,
			44: 3,
			2:  0,
		},
		fluffinessRating: "zero",
	},
}

// 1 POINT
func TestDoggo_Name(t *testing.T) {
	name, err := doggos["nova"].Name()
	td.Cmp(t, name, "Nova")
	td.CmpNoError(t, err)

	name, err = doggos["rover"].Name()
	td.Cmp(t, name, "Rover")
	td.CmpNoError(t, err)

	name, err = doggos["pavlova"].Name()
	td.Cmp(t, name, "")
	td.CmpNoError(t, err)
}

// 2 POINTS
func TestDoggo_Breed(t *testing.T) {
	breed, err := doggos["nova"].Breed()
	td.Cmp(t, breed, "Land Shark")
	td.CmpNoError(t, err)

	breed, err = doggos["rover"].Breed()
	td.Cmp(t, breed, "Good Boy")
	td.CmpNoError(t, err)

	breed, err = doggos["pavlova"].Breed()
	td.Cmp(t, breed, "")
	td.CmpError(t, err)
}

// 4 POINTS
func TestDoggo_Weight(t *testing.T) {
	weight, err := doggos["nova"].Weight()
	td.Cmp(t, weight, 13.6)
	td.CmpNoError(t, err)

	weight, err = doggos["rover"].Weight()
	td.Cmp(t, weight, 23.1)
	td.CmpNoError(t, err)

	weight, err = doggos["pavlova"].Weight()
	td.Cmp(t, weight, 1.3)
	td.CmpNoError(t, err)
}

// 1 POINT
func TestDoggo_Fluffiness(t *testing.T) {
	fluff, err := doggos["nova"].Fluffiness()
	td.Cmp(t, fluff, 10)
	td.CmpNoError(t, err)

	fluff, err = doggos["rover"].Fluffiness()
	td.Cmp(t, fluff, 5)
	td.CmpNoError(t, err)

	fluff, err = doggos["pavlova"].Fluffiness()
	td.Cmp(t, fluff, 0)
	td.CmpNoError(t, err)
}

// 2 POINTS
func TestDoggo_FavoriteTreat(t *testing.T) {
	treat, err := doggos["nova"].FavoriteTreat()
	td.Cmp(t, treat, "Bulls Penis")
	td.CmpNoError(t, err)

	treat, err = doggos["rover"].FavoriteTreat()
	td.Cmp(t, treat, "Gravy Bones")
	td.CmpNoError(t, err)

	treat, err = doggos["pavlova"].FavoriteTreat()
	td.Cmp(t, treat, "")
	td.CmpError(t, err)
}
