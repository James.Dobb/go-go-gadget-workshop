package main

import (
	"errors"
	"fmt"
)
// There is no try catch in go!
// Instead you must return errors and handle them as they occor
// Any panic will immediately halt the execution of a program
// Recovery from panics is possible see: https://gobyexample.com/recover

func main() {
	// normal error handling
	myData, err := doSomething(true)
	if err != nil {
		panic(err)
	}
	fmt.Println(myData)

	// short-hand error handling
	if myData, err = doSomething(true); err != nil {
		panic(err)
	}
	fmt.Println(myData)
}

func doSomething(ok bool) (string, error) {
	if ok {
		return "DATA-OK", nil
	}

	return "", errors.New("oops")
}