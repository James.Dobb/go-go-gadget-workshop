package main

import (
	"fmt"
)

func main()  {
	whileLoops()
	fmt.Println("---", "")

	forLoops()
	fmt.Println("---", "")

	doLoops()
	fmt.Println("---", "")

	rangeLoops()
	fmt.Println("---", "")
}

func whileLoops() {
	i := 1
	for i < 5 {
		fmt.Println(i)
		i++
	}
}

func forLoops()  {
	for i := 1; i <= 4; i++ {
		fmt.Println(i)
	}
}

func doLoops()  {
	i := 0

	for {
		i++
		fmt.Println(i)
		if i >= 4 {
			break
		}
	}
}

func rangeLoops()  {
	x := []int{1,2,3,4}

	for _, v := range x {
		fmt.Println(v)
	}
}


