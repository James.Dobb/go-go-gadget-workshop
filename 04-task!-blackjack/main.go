package main

/*
We're playing Blackjack!

You must complete this function with the following:

 - The return should be 2 values:
	- a total value of the cards added up
   	- & a boolean if we should hit or stand

 - card1 and card2 are passed in as strings and corresponding values of:
		"two" (2)
		"three" (3)
        "four" (4)
 		"five" (5)
		"six" (6)
		"seven" (7)
		"eight" (8)
		"nine" (9)
		"ten" (10)
		"jack" (10),
		"queen" (10),
		"king" (10),
		"ace" (11)

 - if the dealt hand contains 2 aces then for the purposes of this game the second
   ace holds a value of 1

 - any "joker" or similar cards accidentally mixed into the deck are worth -1 per card

 - you're encouraged to make your code condensed and sensible, try and add extra functions as needed
*/

const HitIfLessThan = 17

func ShouldIHit(_ string, _ string) (int, bool) {
	return 0, false
}
