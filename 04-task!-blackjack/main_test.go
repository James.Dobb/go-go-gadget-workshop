package main

import (
	"github.com/maxatome/go-testdeep/td"
	"testing"
)

type scenario struct {
	name              string
	card1             string
	card2             string
	expectedTotal     int
}

func TestShouldIHit(t *testing.T) {
	scenarios := []scenario{
		{"two-aces", "ace", "ace", 12},
		{"sad-trombone", "two", "two", 4},
		{"blackjack!", "ace", "queen", 21},
		{"who-shuffled!?", "joker", "beer-mat", -2},
		{"close-call", "jack", "seven", 17},
		{"picture-cards", "jack", "king", 20},
		{"graffiti", "ace", "space", 10},
	}

	for _, tc := range scenarios {
		t.Run(tc.name, func(t *testing.T) {
			val, hit := ShouldIHit(tc.card1, tc.card2)
			td.Cmp(t, val, tc.expectedTotal)
			td.Cmp(t, hit, tc.expectedTotal < HitIfLessThan)
		})
	}
}
