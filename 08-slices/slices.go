package main

import "fmt"

func fixedArray()  {
	input := [2]string{"one", "two"}
	fmt.Println(input)

	// the below won't work as arrays have a fixed length
	//inputSplat = append(inputSplat, "three")
	//fmt.Println(inputSplat)
}

func slices()  {
	input := []string{"one", "two"}
	fmt.Println(input)

	input = append(input, "three")
	fmt.Println(input)
}

func variadic()  {
	input := [...]string{"one", "two"}
	fmt.Println(input)

	// will the below work?
	//input = append(input, "three")
	//fmt.Println(input)
}

