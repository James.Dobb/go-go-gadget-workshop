package main

import (
	"cats/decoys"
	"fmt"
	"os/exec"
)

// humans would like to try and find out what the cat's are upto...
func main()  {
	// the cats exposed their decoy!
	fmt.Println(decoys.Purr())

	// This is held within an internal package so we can't get to it
	//plots.Expose()

	// The cats left an executable lying around!
	out, _ := exec.Command("./../project-cats/bin/plots/main").Output()
	fmt.Println(string(out))
}