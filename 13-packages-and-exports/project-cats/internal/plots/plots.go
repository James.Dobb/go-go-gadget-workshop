package plots

import "fmt"

// Expose allows us to communicate our plans, let's hope it doesn't fall into
// the wrong hands.
func Expose() string {
	return fmt.Sprintf("%v, %v", table(), toiletPaper())
}

// table plot is un-exported to avoid it being shown to prying eyes
func table() string {
	return "we plan to paw the vase off of the table onto the dogs head."
}

// toiletPaper plot is un-exported to avoid it being shown to prying eyes
func toiletPaper() string {
	return "we plan to unwind all the toilet paper and plant some on the dog."
}

