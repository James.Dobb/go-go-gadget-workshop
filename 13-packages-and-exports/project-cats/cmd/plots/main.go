package main

import (
	"cats/decoys"
	"cats/internal/plots"
	"fmt"
)

// this can be built and will be the entrypoint into our information.
func main() {
	// an exported package method can be accessed.
	fmt.Println(fmt.Sprintf("we shall tell the humans: %v", decoys.Purr()))

	// we can access the Table plot even though its in the internal package.  Access is limited to the module.
	fmt.Println(fmt.Sprintf("our actual plots invole: %v", plots.Expose()))

	//plots.toiletPaper() - we cannot access this method directly due to it being un-exported within its package
}