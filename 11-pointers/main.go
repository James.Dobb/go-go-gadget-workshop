package main

import "fmt"

func main() {
	var i *int // only a pointer to an actual int

	a := 2

	i = &a // point at a which is set to 5

	a = 5

	fmt.Println(i) // this is a pointer to an address, not the actual value

	fmt.Println(*i) // ah - that's more like it! (de-referenced a pointer)
}