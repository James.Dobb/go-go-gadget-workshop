package main

import "fmt"

func main() {
	aSimpleMap()

	fmt.Println("---")

	aMapOfMaps()

	fmt.Println("---")
}

func aSimpleMap() {
	myMap := make(map[string]int)
	myMap["twelve"] = 12
	myMap["two"] = 2

	myOtherMap := map[string]int{
		"four": 4,
		"six": 6,
	}

	// we can check for key existence
	val, exists := myOtherMap["four"]
	if exists {
		fmt.Println(val, "exists at four!")
	} else {
		fmt.Println("nothing exists at four!")
	}

	delete(myOtherMap, "four")

	// again checking for key existence but with shorter hand if
	if val, exists := myOtherMap["four"]; exists {
		fmt.Println(val, "exists at four!")
	} else {
		fmt.Println("nothing exists at four!")
	}

	fmt.Println(myMap)
	fmt.Println(myOtherMap)
}

func aMapOfMaps() {
	mappyMap := make(map[int]map[int]string)
	//mappyMap[12][4] = "uh oh" // this would panic!

	mappyMap[12] = make(map[int]string)
	mappyMap[12][4] = "x marks the spot"

	fmt.Println(mappyMap)
	// ------


	anotherMappyMap := map[int]map[int]string{
		12: {4: "x marks the spot"},
	}

	//anotherMappyMap[16][2] = "uh oh"  // this will also panic!

	fmt.Println(anotherMappyMap)
}
