package main

import "fmt"

/*
	We would like to print out the cars in order of their top speed, the fastest
    being listed first.  This function doesn't seem to do the job.  Modify the function
    to consistently output the cars in the correct order.
 */
func main() {
	cars := map[string]int{
		"SSC Tuatara": 316,
		"Bugatti Chiron Super Sport 300+": 304,
		"Koenigsegg Agera RS": 278,
		"Hennessey Venom GT": 270,
		"Bugatti Veyron Super Sport": 268,
		"Bugatti Chiron": 261,
		"Koenigsegg Agera R": 260,
		"SSC Ultimate Aero": 256,
	}

	for car, topSpeed := range cars {
		fmt.Println(car, topSpeed)
	}
}
