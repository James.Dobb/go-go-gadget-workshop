// all files must declare their package.
// each directory should only contain a single package
// the package will normally be the same as the directory name
// "main" is a special package and will be an app's entrypoint
package main

import (
	"github.com/gin-gonic/gin"
)

/*
main() is combined with the main package as an entrypoint into your app

to create a new project you can run:
 	- go mod init basic

to add a remote dependency you can run (-u also tells go to check for updates to the package):
	- go get -u github.com/gin-gonic/gin

to run this app either (note the former only includes the specified files to run, not OK if you depend on other files):
   - go run main.go
   - go run .

once running visit: http://localhost:8085/ping
 */
func main()  {
	r := gin.Default()
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{"message": "pong"})
	})
	_ = r.Run(":8085")
}

/*
 NOTE! any errors such as: go:linkname must refer to declared function or variable

 run the following to fix the outdated package:
 - go get -u golang.org/x/sys
 */
