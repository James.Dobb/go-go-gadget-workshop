package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println(doSwitch("!"))

	detectAnimals("dog-grape-cat")
}

func doSwitch(i string) string {
	switch i {
	case "hello", "world", "!":
		return "hello, world!"
	case "dog":
		return "woof"
	case "cat":
		return "meow"
	default:
		return "unknown"
	}
}

func detectAnimals(animals string) {
	switch {
	case strings.Contains(animals, "dog"):
		fmt.Println("found a dog!")
		// fallthrough is critical if we want the possibility of multiple cases to match
		fallthrough
	case strings.Contains(animals, "cat"):
		fmt.Println("found a cat!")
	}
}