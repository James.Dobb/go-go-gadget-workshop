package main

import (
	"errors"
	"github.com/maxatome/go-testdeep/td"
	"golf-interfaces/mocks"
	"testing"
	"time"
)

func TestNavigate(t *testing.T) {
	mockCart := &mocks.Cart{}
	mockRecordKeeper := &mocks.RecordKeeper{}

	mockCart.On("NavigateToHole", 3).
		Return(time.Duration(12), errors.New("crashed into a snake"))

	mockCart.On("GetName").Return("Ralph")

	// We expect a call to the record keeper
	// we expect the values to be that returned from the cart's NavigateToHole and GetName methods
	mockRecordKeeper.On("RecordJourney", "Ralph", false, time.Duration(12))

	err := Navigate(mockCart, mockRecordKeeper, 3)
	td.CmpError(t, err)

	// let's ensure we called the expected mocks
	mockCart.AssertNumberOfCalls(t, "GetName", 1)
	mockCart.AssertNumberOfCalls(t, "NavigateToHole", 1)
	mockRecordKeeper.AssertNumberOfCalls(t, "RecordJourney", 1)
}
