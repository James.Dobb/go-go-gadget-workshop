package main

import (
	"golf-interfaces/aicarts"
	"golf-interfaces/classiccarts"
	"golf-interfaces/performance"
	"time"
)

type Cart interface {
	NavigateToHole(h int) (time.Duration, error)
	GetName() string
}

type RecordKeeper interface {
	RecordJourney(cartName string, success bool, timeTaken time.Duration)
}

// Mock tool:
// - go install github.com/vektra/mockery/v2@latest
// - mockery --all --dir ./ --keeptree --output ./mocks
func main() {
	rk := &performance.RecordKeeper{}

	c := &classiccarts.Cart{
		Name:         "joey's cart",
		MaxSpeed:     12,
		ClubCapacity: 22,
	}
	_ = Navigate(c, rk, 2)


	c2 := &aicarts.Cart{
		Name:         "beep boop",
		MaxSpeed:     812,
		ClubCapacity: 42,
	}
	_ = Navigate(c2, rk,3)
}

// Navigate - Note as this in an interface we don't need the "*" for the type!
func Navigate(c Cart, r RecordKeeper, h int) error {
	tth, err := c.NavigateToHole(h)

	r.RecordJourney(c.GetName(), err == nil, tth)

	return err
}
