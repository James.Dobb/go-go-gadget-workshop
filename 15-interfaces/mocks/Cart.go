// Code generated by mockery v2.12.0. DO NOT EDIT.

package mocks

import (
	testing "testing"

	mock "github.com/stretchr/testify/mock"

	time "time"
)

// Cart is an autogenerated mock type for the Cart type
type Cart struct {
	mock.Mock
}

// GetName provides a mock function with given fields:
func (_m *Cart) GetName() string {
	ret := _m.Called()

	var r0 string
	if rf, ok := ret.Get(0).(func() string); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(string)
	}

	return r0
}

// NavigateToHole provides a mock function with given fields: h
func (_m *Cart) NavigateToHole(h int) (time.Duration, error) {
	ret := _m.Called(h)

	var r0 time.Duration
	if rf, ok := ret.Get(0).(func(int) time.Duration); ok {
		r0 = rf(h)
	} else {
		r0 = ret.Get(0).(time.Duration)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(int) error); ok {
		r1 = rf(h)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// NewCart creates a new instance of Cart. It also registers the testing.TB interface on the mock and a cleanup function to assert the mocks expectations.
func NewCart(t testing.TB) *Cart {
	mock := &Cart{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
