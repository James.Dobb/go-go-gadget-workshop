package classiccarts

import (
	"errors"
	"time"
)

type Cart struct {
	Name         string
	MaxSpeed     float64
	ClubCapacity int
	aiClient     interface{}
}

func (c *Cart) GetName() string {
	return c.Name
}

// NavigateToHole asks the driver to navigate to the location.
func (c *Cart) NavigateToHole(h int) (time.Duration, error) {
	if h == 1 {
		return time.Second * 40, nil
	} else if h == 2 {
		return time.Second * 90, nil
	} else if h == 3 {
		return 0, errors.New("the cart crashed into an elephant")
	}

	return 0, errors.New("unknown destination")
}
