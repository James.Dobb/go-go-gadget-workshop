package performance

import (
	"fmt"
	"time"
)

type RecordKeeper struct{}

func (rc *RecordKeeper) RecordJourney(cartName string, success bool, timeTaken time.Duration) {
	fmt.Println(cartName, success, timeTaken)
}
